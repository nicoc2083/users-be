# USERS-BE

## PREPARACION DE AMBIENTE

1.- Actualmente existen dos modos de poder ejecutar la aplicacion a nivel de integración con Base de Datos HSQL

	a.- Utilizar la Base de Datos que tiene el proyecto 
	b.- Instanciar a una base de Datos HSQL externa.

- Para el punto a el proyecto ya hace referencia a la BD interna por lo que **solo se debe ejecutar el comando del punto 1** 
y la aplicacion funcionará inmediatamente ya que ella se encargará de crear los objetos en base de datos y se puede dirigir inmediatamente al punto de ejecución.


- Para el punto b se debe descargar la BD HSQL desde el sitio https://sourceforge.net/projects/hsqldb/files/ y elegir descargar la ultima version (2.5.0), esto descargará un zip 
el cual debemos llevar la raiz del disco duro y descomprimir (se usará como referencia el disco d) esto generará una serie de carpetas y en la ruta 
D:\hsqldb-2.5.0\hsqldb-2.5.0\hsqldb deberemos copiar el archivo server.properties que se encuentra en el proyecto en la carpeta configurationDB
luego en est misma  ruta para iniciar la BD se debe
ejecutar el comando java -classpath lib/hsqldb.jar org.hsqldb.server.Server
Posterior se debe conectar a la BD con algun editor (de preferencia DBeaver) Utilizando los siguientes datos de conexion 

- **JDBC URL: jdbc:hsqldb:hsql://localhost:9001/userdb**
- **HOST: localhost**
- **Port: 9001**
- **Database/Schema: userdb**
- **Username: SA**
- **Password:**

Posterior a esto y ya conectado a BD con el editor se puede proceder a ejecutar el script de creacion de tablas USERS y PHONES
Los archivos se encuentran en carpeta **configurationDB** dentro del proyecto (DB.sql)

Actualmente el proyecto está configurado para crear los objetos en BD es por ello que se debe deshabilitar esto para ello se debe modificar el archivo application.properties
que se encuentra en ruta: /users-app/src/main/resources/application.properties

- Se debe comentar esta línea
#spring.datasource.url=jdbc:hsqldb:mem:userdb;DB_CLOSE_DELAY=-1
- Posterior descomentar esta línea
spring.datasource.url=jdbc:hsqldb:hsql://localhost/userdb
- por ultimo comentar esta linea
#spring.jpa.hibernate.ddl-auto=update

Posterior a esto se puede conectar a un servidor de Base de Datos HSQL.

## EJECUCION

Para iniciar se debe ejecutar comando $ ./gradlew bootRun en la raiz del proyecto

## PRUEBAS 

La URL del Servicio es **http://localhost:8080/api/v1/users/**
En la ruta users-app\src\main\resources se deja el archivo Users.postman_collection.json
el cual tiene un request asociado al proyecto que se puede utilizar con postman para pruebas.


## DIAGRAMA DE SOLUCION 

Este Diagrama se encuentra en el proyecto en ruta /users-app/configurationDB/Diagrama_Solucion.jpg