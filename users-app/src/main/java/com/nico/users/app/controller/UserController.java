package com.nico.users.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nico.users.app.dto.UserDTO;
import com.nico.users.app.dto.UserResponse;
import com.nico.users.app.entity.User;
import com.nico.users.app.exception.InvalidUserException;
import com.nico.users.app.service.UsersService;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

	@Autowired
	private UsersService userService;
	
	@PostMapping("/")
	  public UserResponse createUser(@RequestBody @Valid UserDTO user,Errors errors) throws InvalidUserException {
		if (errors.hasErrors()) {
			throw new InvalidUserException(errors);
	    }
		
		return userService.createUser(user);
	}
	
	@GetMapping("/")
	public List<User> allUsers() {
	    return userService.getUsers();
	 }
	
	@GetMapping("/{id}")
	public Object findUserById(@PathVariable String id) {
	    return userService.findById(id);
	  }
	
	@DeleteMapping("/{id}")
	public @ResponseBody void  deleteUserById(@PathVariable String id) {
	    userService.deleteById(id);
	  }
	
	@PutMapping("/{id}")
	public @ResponseBody User  updateUser(@RequestBody User newUser,@PathVariable String id) throws InvalidUserException {
		return userService.updateUser(newUser,id);
	}
	
	
}
