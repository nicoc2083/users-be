package com.nico.users.app.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.nico.users.app.entity.Phone;

@Repository
public interface PhoneRepository extends CrudRepository<Phone, Integer>{

}
