package com.nico.users.app.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nico.users.app.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, String>{
	

    @Query("SELECT count(1) FROM User  where EMAIL = :email") 
    Integer findCountByEmail(@Param("email") String email);

}
