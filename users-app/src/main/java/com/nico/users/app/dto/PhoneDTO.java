package com.nico.users.app.dto;

import javax.validation.constraints.NotBlank;

public class PhoneDTO {
	@NotBlank(message = "El n�mero es obligatorio")
	private String number;
	@NotBlank(message = "cityCode es obligatorio")
	private String citycode;
	@NotBlank(message = "CountryCode es obligatorio")
	private String countrycode;
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getCitycode() {
		return citycode;
	}
	public void setCitycode(String citycode) {
		this.citycode = citycode;
	}
	public String getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	} 

}
