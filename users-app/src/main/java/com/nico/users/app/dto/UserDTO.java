package com.nico.users.app.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class UserDTO {

	@NotBlank(message = "El nombre es obligatorio")
	private String name;
	@NotBlank(message = "Email es obligatorio")
	private String email;
	@NotBlank(message = "Password es obligatorio")
	private String password;
	@NotEmpty(message = "La lista de T�lefonos no puede ser vac�a.")
	private List<PhoneDTO> phones;

	private static String EMAIL_PATTERN = "^\\w+@[a-zA-Z]+\\.[a-zA-Z]{2,3}$";
	private static String PASSWORD_PATTERN = "^(?=.*([A-Z]))(?=.*([a-z].*[a-z]))(?=.*([0-9].*[0-9])).*$";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<PhoneDTO> getPhones() {
		return phones;
	}

	public void setPhones(List<PhoneDTO> phones) {
		this.phones = phones;
	}

	public boolean validEmail() {
		return email.matches(EMAIL_PATTERN);
	}

	public boolean validPassword() {
		return password.matches(PASSWORD_PATTERN);
	}

}
