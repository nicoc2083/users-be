package com.nico.users.app.dto;

import java.sql.Date;

public class UserResponse {
	private UserDTO usuario;
	private Date createdDate;
	private Date modifiedDate;
	private String TokenUser;
	private Boolean isActive;
	private String idUsuario;
	public UserDTO getUsuario() {
		return usuario;
	}
	public void setUsuario(UserDTO usuario) {
		this.usuario = usuario;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getTokenUser() {
		return TokenUser;
	}
	public void setTokenUser(String tokenUser) {
		TokenUser = tokenUser;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	@Override
	public String toString() {
		return "UserResponse [usuario=" + usuario + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate
				+ ", TokenUser=" + TokenUser + ", isActive=" + isActive + "]";
	}
	
	
	
	
}

