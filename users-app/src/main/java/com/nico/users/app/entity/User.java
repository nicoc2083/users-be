package com.nico.users.app.entity;

import java.sql.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "USERS")
public class User {
	@Id
	private String idUser;
	private String name;
	@NaturalId
	@Column(name="EMAIL")
	private String email;
	private String password;
	private Date createdDate;
	private Date modifiedDate;
	private String TokenUser;
	private Boolean isActive;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Phone> phones;
	
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getTokenUser() {
		return TokenUser;
	}
	public void setTokenUser(String tokenUser) {
		TokenUser = tokenUser;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	@Override
	public String toString() {
		return "User [IdUser=" + idUser + ", name=" + name + ", email=" + email + ", password=" + password
				+ ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + ", TokenUser=" + TokenUser
				+ ", isActive=" + isActive + "]";
	}
	public List<Phone> getPhones() {
		return phones;
	}
	public void setPhones(List<Phone> phones) {
		this.phones = phones;
	}
	
	
	
}
