package com.nico.users.app.exception;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class InvalidUserException extends RuntimeException {

	private static final long serialVersionUID = 8058996327672885158L;
	private String message;

	public static final String INVALID_PASSWORD = "Clave incorrecta. La clave debe presentar al menos una may�scula, letras min�sculas, y dos n�meros";
	public static final String INVALID_EMAIL = "El formato de correo es incorrecto";
	public static final String DUPLICATE_EMAIL = "El correo ya se encuentra registrado";
	public static final String NO_DATA_FOUND = "No se encontraron Datos";
	public InvalidUserException() {};
	
	public InvalidUserException(String errorMessage) {
		super(errorMessage);
		this.setMessage(errorMessage);
	}

	public InvalidUserException(Errors errors) {
		StringBuilder errores=new StringBuilder();
		for (ObjectError error: errors.getAllErrors()) {
			errores.append(error.getDefaultMessage()).append(" - ");
		}
		this.setMessage(errores.toString());
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
