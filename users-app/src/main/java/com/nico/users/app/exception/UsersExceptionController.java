package com.nico.users.app.exception;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class UsersExceptionController extends ResponseEntityExceptionHandler {

	public static final String MESSAGE_KEY = "mensaje";

	@ExceptionHandler(InvalidUserException.class)
	public final ResponseEntity<Map<String, Object>> handleAllExceptions(InvalidUserException exception) {
		Map<String, Object> errorMessage = new LinkedHashMap<>();
		errorMessage.put(MESSAGE_KEY, exception.getMessage());
		return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
	}
	
	 @Override
	   protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
	       String error = "Request Inv�lido";
	       Map<String, Object> errorMessage = new LinkedHashMap<>();
			errorMessage.put(MESSAGE_KEY, error);
			return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
		
	   }

	   
	
}
