package com.nico.users.app.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nico.users.app.dao.PhoneRepository;
import com.nico.users.app.dao.UserRepository;
import com.nico.users.app.dto.PhoneDTO;
import com.nico.users.app.dto.UserDTO;
import com.nico.users.app.dto.UserResponse;
import com.nico.users.app.entity.Phone;
import com.nico.users.app.entity.User;
import com.nico.users.app.exception.InvalidUserException;

@Service
public class UsersService {
	@Autowired
	private UserRepository userRepository;
	@SuppressWarnings("unused")
	@Autowired
	private PhoneRepository phoneRepository;

	private static final Logger logger = LogManager.getLogger(UsersService.class);

	public UserResponse createUser(UserDTO user) throws InvalidUserException {
		logger.info("Llamando Metodo Insert");

		if (!user.validEmail()) {
			throw new InvalidUserException(InvalidUserException.INVALID_EMAIL);
		}
		if (!user.validPassword()) {
			throw new InvalidUserException(InvalidUserException.INVALID_PASSWORD);
		}

		// Validar si el email ya existe
		Integer countMail = userRepository.findCountByEmail(user.getEmail());

		if (countMail > 0) {
			throw new InvalidUserException(InvalidUserException.DUPLICATE_EMAIL);
		}

		User userEnt = new User();
		buildUserEnt(user, userEnt);
		userRepository.save(userEnt);
		UserResponse userResponse = new UserResponse();
		buildResponse(userEnt.getIdUser(), user, userResponse);
		return userResponse;

	}
	

	private void buildResponse(String idUser,UserDTO user, UserResponse userResponse) {
		User userDb = userRepository.findById(idUser).orElseGet(null);

		userResponse.setUsuario(user);
		userResponse.setCreatedDate(userDb.getCreatedDate());
		userResponse.setIsActive(userDb.getIsActive());
		userResponse.setModifiedDate(userDb.getModifiedDate());
		userResponse.setTokenUser(userDb.getTokenUser());
		userResponse.setIdUsuario(userDb.getIdUser());
	}

	private void buildUserEnt(UserDTO user, User userEnt) {
		List<Phone> listPhones=new ArrayList<Phone>();
		userEnt.setIdUser(UUID.randomUUID().toString());
		userEnt.setName(user.getName());
		userEnt.setPassword(user.getPassword());
		userEnt.setTokenUser(userEnt.getIdUser());
		userEnt.setIsActive(Boolean.TRUE);
		userEnt.setEmail(user.getEmail());
		java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		userEnt.setCreatedDate(sqlDate);
		userEnt.setModifiedDate(sqlDate);
		if (user.getPhones() != null) {
			for (PhoneDTO phone : user.getPhones()) {
				Phone phoneEnt = new Phone();
				phoneEnt.setNumberPhone(phone.getNumber());
				phoneEnt.setCityCode(phone.getCitycode());
				phoneEnt.setCountryCode(phone.getCountrycode());
				listPhones.add(phoneEnt);
			}
		}
		userEnt.setPhones(listPhones);
	}

	public List<User> getUsers() {
		List<User> listaUsuarios= (List<User>) userRepository.findAll();
		logger.info("Usuarios" +listaUsuarios.toString());
		if(listaUsuarios.isEmpty()) {
			throw new InvalidUserException(InvalidUserException.NO_DATA_FOUND);
		}else {
			//Crear Response
			return listaUsuarios;
		}
	}

	public Object findById(String id) {
		return userRepository.findById(id).orElseThrow(() ->new InvalidUserException(InvalidUserException.NO_DATA_FOUND));
	}


	public void deleteById(String id) {
		userRepository.findById(id).orElseThrow(() ->new InvalidUserException(InvalidUserException.NO_DATA_FOUND));
		userRepository.deleteById(id);
	}


	public User updateUser(User newUser, String id) {
		UserDTO userDTO= new UserDTO();
		userDTO.setPassword(newUser.getPassword());
		if (newUser.getPassword()!=null && !userDTO.validPassword()) {
			throw new InvalidUserException(InvalidUserException.INVALID_PASSWORD);
		}
		User user= userRepository.findById(id).orElseThrow(() ->new InvalidUserException(InvalidUserException.NO_DATA_FOUND));
  	  	user.setName(newUser.getName()==null?user.getName():newUser.getName());
  	  	user.setPassword(newUser.getPassword()==null?user.getPassword():newUser.getPassword());
  	    user.setIsActive(newUser.getIsActive()==null?user.getIsActive():newUser.getIsActive());
	  	java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
	  	user.setModifiedDate(sqlDate);
  	  	
	  	if(newUser.getPhones()!=null) {
	  		user.setPhones(newUser.getPhones());
	  	}
	  	
	  	return userRepository.save(user);
		
	}
}
