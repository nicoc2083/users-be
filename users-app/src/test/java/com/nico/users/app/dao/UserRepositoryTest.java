package com.nico.users.app.dao;

import java.util.Calendar;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.nico.users.app.entity.User;
import com.nico.users.app.service.UsersService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserRepositoryTest {
	   
@Autowired
private UserRepository userRepository;
	
private static final Logger logger = LogManager.getLogger(UserRepositoryTest.class);

	@Test
	public void testSave() {
		User user=new User();
		user.setIdUser("1");
		user.setName("Nico");
		java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		user.setCreatedDate(sqlDate);
		user.setModifiedDate(sqlDate);
		user.setEmail("a@a.cl");
		user.setPassword("Password");
		user.setTokenUser("43455");
		user.setIsActive(true);
		userRepository.save(user);
		Optional<User> user2 = userRepository.findById("1");
		logger.info(user2);
	}
	
	@Test
	public void testFindByEmail() {
		User user=new User();
		user.setEmail("oshualdo@gmail.com");
		Integer contador=userRepository.findCountByEmail(user.getEmail());
		logger.info("Count "+contador.toString());
	}
	

}
