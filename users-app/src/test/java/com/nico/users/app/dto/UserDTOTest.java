package com.nico.users.app.dto;

import org.junit.Assert;
import org.junit.Test;

public class UserDTOTest {

	@Test
	public void testValidEmail() {
		UserDTO user=new UserDTO();
		user.setEmail("Correo@correo.cl");
		Assert.assertEquals(Boolean.TRUE, user.validEmail());
	}
	
	@Test
	public void testInvalidEmail() {
		UserDTO user=new UserDTO();
		user.setEmail("Correo@correo.c");
		Assert.assertEquals(Boolean.FALSE, user.validEmail());
	}
	
	@Test
	public void testInvalidEmailDomain() {
		UserDTO user=new UserDTO();
		user.setEmail("Correo@111");
		Assert.assertEquals(Boolean.FALSE, user.validEmail());
	}
	
	@Test
	public void testMalformedEmail() {
		UserDTO user=new UserDTO();
		user.setEmail("Correo");
		Assert.assertEquals(Boolean.FALSE, user.validEmail());
	}
	
	@Test(expected = NullPointerException.class)
	public void testNullEmail() {
		UserDTO user=new UserDTO();
		user.setEmail(null);
		Assert.assertEquals(Boolean.FALSE, user.validEmail());
	}
	

	@Test
	public void testValidPassword() {
		UserDTO user=new UserDTO();
		user.setPassword("Pass150");
		Assert.assertEquals(Boolean.TRUE, user.validPassword());
	}
	
	@Test
	public void testUpperInvalidPassword() {
		UserDTO user=new UserDTO();
		user.setPassword("pass150");
		Assert.assertEquals(Boolean.FALSE, user.validPassword());
	}
	
	@Test
	public void testLowerInvalidPassword() {
		UserDTO user=new UserDTO();
		user.setPassword("pass");
		Assert.assertEquals(Boolean.FALSE, user.validPassword());
	}
	
	@Test
	public void testNumberInvalidPassword() {
		UserDTO user=new UserDTO();
		user.setPassword("12345");
		Assert.assertEquals(Boolean.FALSE, user.validPassword());
	}
	
	@Test(expected = NullPointerException.class)
	public void testNullPassword() {
		UserDTO user=new UserDTO();
		user.setPassword(null);
		Assert.assertEquals(Boolean.FALSE, user.validPassword());
	}

}
